from unittest import TestCase

from UserPackage import MockUserRepositoryBackend, UserRepository


class TestUserRepository(TestCase):
    def setUp(self):
        self.backend = MockUserRepositoryBackend()
        self.repository = UserRepository(self.backend)

    def test_get_all(self):
        self.assertGreaterEqual(len(self.repository.get_all()), 1, "Mock repository should fetch at least one user")

    def test_find(self):
        self.assertIsNotNone(self.repository.find(1), "Mock repository should fetch a user with id=1")
