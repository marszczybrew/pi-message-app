from MockMessageRepositoryBackend import MockMessageRepositoryBackend
from ApiMessageRepositoryBackend import ApiMessageRepositoryBackend

class MessageRepository:
    def __init__(self, mock=False):
        if mock:
            self.backend = MockMessageRepositoryBackend()
        else:
            self.backend = ApiMessageRepositoryBackend()

    def get_for_user(self, user):
        return self.backend.get_for_user(user)

    def remove_message(self, message):
        return self.backend.remove_message(message)
