from MessageRepositoryBackend import MessageRepositoryBackend

class ApiMessageRepositoryBackend(MessageRepositoryBackend):
    def get_for_user(self, user):
        raise NotImplementedError()

    def remove_message(self, message):
        raise NotImplementedError()
