from Message import Message
from MessageRepositoryBackend import MessageRepositoryBackend


class MockMessageRepositoryBackend(MessageRepositoryBackend):
    def __init__(self):
        self.messages = [
            Message(1, "First test message from chaberb to markiem1", "Test message #1", 1, 2, True),
            Message(2, "Another test message, this one is from markiem1 to chaberb.", "Test message #2", 2, 1, True)
        ]

    def remove_message(self, message):
        if message is not Message:
            raise TypeError("message should be an instance of Message")
        self.messages.remove(message)

    def get_for_user(self, user):
        for_user = {'received': [], 'sent': []}
        for message in self.messages:
            if message.from_user_id == user.id:
                for_user['sent'].append(message)
            if message.to_user_id == user.id:
                for_user['received'].append(message)
        return for_user
