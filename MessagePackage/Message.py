class Message:
    def __init__(self, id, content, title, to_user, from_user, unread):
        self.id = id
        self.content = content
        self.title = title
        self.to_user_id = to_user
        self.from_user_id = from_user
        self.unread = unread

    def mark_as_read(self):
        self.unread = False

    def mark_as_unread(self):
        self.unread = True

    def get_data(self):
        """
        Used for serialization and persistence
        :return:
        """
        return dict(
            id=self.id,
            content=self.content,
            title=self.title,
            to_user_id=self.to_user_id,
            from_user_id=self.from_user_id,
            unread=self.unread)
