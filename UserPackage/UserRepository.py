import time

from UserPackage.ApiUserRepositoryBackend import ApiUserRepositoryBackend
from UserPackage.MockUserRepositoryBackend import MockUserRepositoryBackend


class UserRepository:
    def __init__(self, mock=False):
        if mock:
            self.backend = MockUserRepositoryBackend()
        else:
            self.backend = ApiUserRepositoryBackend()
        self.last_access_time = None
        self.users = []

    def is_cache_valid(self):
        if self.last_access_time is None:
            return False
        return time.time() - self.last_access_time < 60

    def get_all(self):
        if self.is_cache_valid():
            return self.get_cache()
        else:
            return self.refresh_cache()

    def set_cache(self, users):
        self.users = users
        self.last_access_time = time.time()

    def get_cache(self):
        return self.users

    def refresh_cache(self):
        self.users = self.backend.get_all()
        self.last_access_time = time.time()
        return self.users

    def find(self, uid):
        for user in self.get_all():
            if user.id == uid:
                return user
        raise Exception("User not found")

    def find_username(self, username):
        for user in self.get_all():
            if user.username == username:
                return user
        raise Exception("user not found")