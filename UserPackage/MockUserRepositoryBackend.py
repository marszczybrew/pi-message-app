from User import User
from UserRepositoryBackend import UserRepositoryBackend

class MockUserRepositoryBackend(UserRepositoryBackend):
    def get_all(self):
        return [User(1, 'chaberb'), User(2, 'markiem1')]
