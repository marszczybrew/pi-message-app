from UserRepositoryBackend import UserRepositoryBackend


class ApiUserRepositoryBackend(UserRepositoryBackend):
    def get_all(self):
        raise NotImplemented()
