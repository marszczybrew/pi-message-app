from flask import Flask, render_template, session, request, abort, redirect, url_for
import string, random

from AuthPackage import AuthService
from MessagePackage import MessageRepository
from UserPackage import UserRepository

app = Flask(__name__)

app.secret_key = 'dosahooghgfsvmia'
app.mock = True
app.user_repository = UserRepository(app.mock)
app.message_repository = MessageRepository(app.mock)
app.auth_service = AuthService(app.mock)


# region Helpers
@app.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.pop('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)


def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = random_string(20)
    return session['_csrf_token']


def random_string(length=10):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(length));


app.jinja_env.globals['csrf_token'] = generate_csrf_token


# endregion


# region Routes
@app.route('/')
def inbox():
    if not app.auth_service.is_logged_in():
        return redirect(url_for('login'))
    return render_template('inbox.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    if app.auth_service.is_logged_in():
        return redirect(url_for('inbox'))

    if request.method == "POST":
        if app.auth_service.do_login(request.form['username'], request.form['password']):
            return redirect(url_for('inbox'))
        else:
            return render_template('login.html', message="Invalid credentials")

    return render_template('login.html')


@app.route('/logout')
def logout():
    app.auth_service.logout()
    return redirect(url_for('login'))


# endregion


if __name__ == '__main__':
    app.run()
