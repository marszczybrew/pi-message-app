from flask import session


class AuthService(object):
    def __init__(self, mock=False):
        if mock:
            self.backend = MockAuthBackend()
        else:
            self.backend = ApiAuthBackend()

    def is_logged_in(self):
        return 'username' in session

    def check_login(self, username, password):
        return self.backend.verify(username, password)

    def do_login(self, username, password):
        try:
            uid = self.backend.verify(username, password)
        except AuthException:
            return False
        session['username'] = username
        session['user_id'] = uid
        return True

    def logout(self):
        if self.is_logged_in():
            session.clear()


class MockAuthBackend(object):
    users = {'chaberb': 1, 'markiem1': 2}

    def verify(self, username, password):
        if username not in self.users:
            raise AuthException("Login failed")
        return self.users[username]


class ApiAuthBackend(object):
    def verify(self, username, password):
        raise NotImplementedError()


class AuthException(Exception):
    pass
